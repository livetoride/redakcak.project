<?php

use AntoninRykalsky\Cms\ControlConfig as CC;

class MoneypointCmsControlsService extends CmsControlsService implements ICmsControlsService
{
	public function __construct() {
		$this->addCmsControls( new CC\ArticleControl );
		$this->addCmsControls( new CC\MultigridControl );
		$this->addCmsControls( new CC\GalleryControl );
		$this->addCmsControls( new CC\FacebookCommentControl );
//		$this->addCmsControls( new CC\CalendarControl );
		$this->addCmsControls( new CC\CarouselControl );
		$this->addCmsControls( new CC\CommentControl );
		$this->addCmsControls( new CC\PollControl );
		$this->addCmsControls( new CC\YoutubeControl );
		$this->addCmsControls( new CC\FacebookLikeControl );
	}
	
	
}

?>