<?php

namespace AntoninRykalsky;
use PDO;

class MyDatabase extends \AntoninRykalsky\EnvConfiguration {

	public function getParams( $dbParams = null )
	{
		if( empty( $this->params ))
		{
			$confKey = 'localhost';
			if( \Nette\Environment::isConsole() || empty( $_SERVER['SERVER_NAME'] ))
			{
				if( $_SERVER['HOSTNAME'] === 'softwarestudio.cz' ) {
					$confKey = 'xmoneypoint';
				} else {
					$confKey = 'localhost';
				}
				
			} else {
				$_SERVER['SERVER_NAME'] == 'localhost' ? $confKey = self::LOCAL:null;
				$_SERVER['SERVER_NAME'] == 'beta.moneypoint.cz' ? $confKey = self::BETA:null;
				$_SERVER['SERVER_NAME'] == 'xmoneypoint.mlm-soft.cz' ? $confKey = 'xmoneypoint':null;
				$_SERVER['SERVER_NAME'] == 'moneypoint.cz' ? $confKey = self::PRODUCTION:null;
				$_SERVER['SERVER_NAME'] == 'www.moneypoint.cz' ? $confKey = self::PRODUCTION:null;
				$_SERVER['SERVER_NAME'] == 'moneypoint.local' ? $confKey = self::LOCAL:null;
			}
			$this->params = $dbParams[$confKey] ;
		}
		return $this->params;
	}
}

?>